module.exports.run = (client, message, args) => {
var args = args.slice(1).join(" ");

console.log(args);
function doMagic8BallVoodoo() {
    var rand = ['Yes', 'No', 'Why are you even trying?', 'What do you think? NO', 'Maybe', 'Never', 'Yep'];

    return rand[Math.floor(Math.random()*rand.length)];
}

if(args == ""){
    message.channel.send("The Magical Eight Ball requires a query. Give it a query or perish!");
    return;
}

message.channel.send({embed: {
    color: 3447003,
    title: ":8ball: **8ball**",
    description: "It's magic time.",
    fields: [{
        name: "You said...",
        value: " " + args + " "
      },
      {
        name: "The 8ball said...",
        value: doMagic8BallVoodoo()
       }
     ]
  }
});

};
module.exports.conf = {
    name: "8ball",
    desc: "Consult the Eight Ball to answer your questions!",
    howtouse: [ "8ball $QUERY" ]
};
