module.exports.run = (client, message, args) => {
	let cmd = args[1];

	// DEBUG message.channel.send("Here's what I saw in args[1]: " + cmd);

	if (!cmd || cmd == null | cmd == "") helplist("mainhelp");
	else helplist("cmdhelp");



	function helplist(show) {
		if (show == "mainhelp") {
			var currcmds = [];
			client.commands.forEach(c => { currcmds.push("`" + client.config.prefix + c.conf.name + "`"); });
			message.channel.send({embed: {
				title: "**Spybot's Stolen List-All-Command-Code List**",
				color: 3447003,
				description: "Spybot Command List",
				fields: [
					{ "name": "Available Commands", "value": currcmds.join("\n") },
					{ "name": "Don't need a list of commands?", "value": "Execute `" + client.config.prefix + "help commandhere`" }
				]
			}});
		} else if (show == "cmdhelp") {
			let command = client.commands.get(cmd);

			if (!command.conf.name || command.conf.name == null || command.conf.name == "") return message.channel.send('ok dingus, not a real command');
			else {

				var htuse = [];
				if (command.conf.howtouse == null) htuse.push("`Cannot find usage information.`");
				else if (command.conf.howtouse == "NONE") htusepush("`" + client.config.global.prefix + command.conf.name + "`");
				else command.conf.howtouse.forEach(use => { htuse.push("`" + client.config.global.prefix + use + "`"); });

				message.channel.send("How to use " + command.conf.name + ": " + htuse.join("\n"));

				message.channel.send({embed: {
					title: "**Spybot's Stolen List-All-Command-Code List**",
					color: 3447003,
					description: "Spybot Command List",
					fields: [
						{ "name": "Command name", "value": command.conf.name },
						{ "name": "Command description", "value": command.conf.desc },
						{ "name": "How to use " + command.conf.name, "value": htuse.join("\n") }
					]
				}});
			};
		} else message.channel.send("something's not right. `You called helplist()` with arg `" + show + "` which is not usable in this function.");
	};
}

module.exports.conf = {
	name: "help",
	desc: "New and improved! Now lists every command and its now-not-hidden description!",
	howtouse: [ "help \[command\]" ]
};
