module.exports.run = (client, message, args) => {
var target = args.slice(1).join(" ");

message.channel.send('https://imgur.com/a/IR5Hljw')
message.channel.send({embed: {
    color: 3447003,
    title: "Board Smashing",
    description: "MY protocols require " + target + " to be [board smashed.](https://cdn.discordapp.com/attachments/569616262309871631/635405387285135370/board_smash_img.png)"
    }
  })
};



module.exports.conf = {
	name: "boardsmash",
	desc: "MY protocols require you to be board smashed!",
    howtouse: [ "boardsmash $TARGET" ]
};
