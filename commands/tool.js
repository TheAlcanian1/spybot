module.exports.run = (client, message, args) => {

if (message.author.id === '208129127494975488') {
 try {
        const code = message.content.split(" ").slice(1).join(" ");
        let evaled = eval(code);

        if (typeof evaled !== "string") evaled = require("util").inspect(evaled);

        if (evaled !== "Promise { <pending> }") message.channel.send(clean(evaled), {code:"xl"});
    } catch (err) {
        // Handle error here
    };
    return;
}
}


module.exports.conf = {
	name: "tool",
	desc: "Shardion's multitool. Can be used for fixing things, breaking things, whatever he says.",
    howtouse: "NONE"
};
