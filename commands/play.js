module.exports.run = (client, message, args) => {
const streamOptions = { seek: 0, volume: 1 };
const ytdl = require('ytdl-core');
var target = args.slice(1).join(" ");
var voiceChannel = message.member.voiceChannel;
        voiceChannel.join().then(connection => {
            console.log("joined channel");
            const stream = ytdl(target, { filter : 'audioonly' });
            const dispatcher = connection.playStream(stream, streamOptions);
            dispatcher.on("end", end => {
                console.log("left channel");
                voiceChannel.leave();
            });
        }).catch(err => console.log(err));
};   
  module.exports.conf = {
	name: "play",
	desc: "Uses the POWAHHH of the SpyTech Radio to play some sort of song.",
    howtouse: [ "radio $YOUTUBE_LINK" ]
};      
